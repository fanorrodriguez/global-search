# Global Search

Este es un proyecto de practica utilizando docker y go

#

### Archivo de configuración

1. Archivo .env

   Aquí se establece la variable PORT, que sera el puerto que esta escuchando la aplicación

2. Archivo app.conf

   Ubicado dentro del directorio config/nginx, donde se establece la configuración básica de nginx

### Instrucciones

1. Clonar proyecto

   ```
   git clone git@bitbucket.org:fanorrodriguez/global-search.git
   ```

2. Acceder al directorio

   ```
   cd global-search
   ```

3. Construir imagen de docker

   ```
   docker-compose build
   ```

4. Crear contenedor

   ```
   docker-compose up -d
   ```

#

### Rutas disponibles

1. /ping

   Retorna el mensaje "pong"

2. /search

   Retorna una lista de con las coincidencia según el parámetro de búsqueda.

   El parámetro "q" es requerido
