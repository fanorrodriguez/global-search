package handlers

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"local/models"
	"net/http"
)

var cache = make(map[string][]models.Result)

func PingHandler(w http.ResponseWriter, r *http.Request) {
	msg := make(map[string]string)
	msg["message"] = "pong"
	j, _ := json.Marshal(msg)
	w.Header().Set("Content-Type", "application/json")
	w.Write(j)
}

func SearchHandler(w http.ResponseWriter, r *http.Request) {
	msg := make(map[string]string)

	q := r.URL.Query().Get("q")

	if q == "" {
		msg["message"] = "Param q is required"
		j, _ := json.Marshal(msg)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(j)
		return
	}

	results, ok := cache[q]

	if !ok {
		sendRequest(q)
		results = cache[q]
	}

	j, _ := json.Marshal(results)

	w.Header().Set("Content-Type", "application/json")
	w.Write(j)
}

func sendRequest(q string) {
	c := make(chan []models.Result, 3)
	results := []models.Result{}

	go sendItunesRequest(q, 1, c)
	go sendItunesRequest(q, 2, c)
	go sendTVMazeRequest(q, c)
	go sendCRCindRequest(q, c)

	for i := 0; i < 3; i++ {
		results = append(results, <-c...)
	}

	cache[q] = results
}

func sendItunesRequest(param string, option int, c chan []models.Result) {
	URL := fmt.Sprintf("https://itunes.apple.com/search?term=%s&media=music", param)
	if option == 2 {
		URL = fmt.Sprintf("https://itunes.apple.com/search?term=%s&media=movie", param)
	}

	resp, _ := http.Get(URL)
	body, _ := ioutil.ReadAll(resp.Body)
	parser := models.TrackResolver{}
	json.Unmarshal(body, &parser)
	results := []models.Result{}
	for _, track := range parser.Results {
		type_ := "Music"
		if track.Kind == "feature-movie" {
			type_ = "Movie"
		}

		result := models.Result{ID: track.TrackId, Name: track.TrackName, Type: type_, URL: track.PreviewUrl, Source: "itunes"}
		results = append(results, result)
	}
	c <- results
}

func sendTVMazeRequest(param string, c chan []models.Result) {
	URL := fmt.Sprintf("https://api.tvmaze.com/search/shows?q=%s", param)
	resp, _ := http.Get(URL)
	body, _ := ioutil.ReadAll(resp.Body)
	parser := models.TVShowResolver{}
	json.Unmarshal(body, &parser)
	results := []models.Result{}
	for _, tvshow := range parser {
		result := models.Result{ID: tvshow.Show.ID, Name: tvshow.Show.Name, Type: "TVShow", URL: tvshow.Show.URL, Source: "TVMaze"}
		results = append(results, result)
	}
	c <- results
}

func sendCRCindRequest(param string, c chan []models.Result) {
	URL := fmt.Sprintf("http://www.crcind.com/csp/samples/SOAP.Demo.cls?soap_method=GetByName&name=%s", param)
	resp, _ := http.Get(URL)
	body, _ := ioutil.ReadAll(resp.Body)
	parser := models.PersonResolver{}
	xml.Unmarshal(body, &parser)

	results := []models.Result{}
	result := parser.ToResult()

	if result.Name != "" {
		results = append(results, result)
	}

	c <- results
}
