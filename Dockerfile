FROM golang:1.18 as builder

WORKDIR /build
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -gcflags=-trimpath=$PWD -o main .

FROM alpine:3.15 as runner

WORKDIR /app
COPY --from=builder /build/main /app/

CMD ["./main"]
