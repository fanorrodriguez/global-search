package models

import (
	"encoding/xml"
	"strconv"
)

type Result struct {
	ID     int
	Name   string
	Type   string
	URL    string
	Source string
}

type TVShow struct {
	ID   int
	Name string
	URL  string
}

type Track struct {
	TrackId    int
	Kind       string
	TrackName  string
	PreviewUrl string
}

type TrackResolver struct {
	Results []Track
}

type TVShowResolver []struct {
	Show TVShow
}

type PersonResolver struct {
	XMLName xml.Name `xml:"Envelope"`
	Body    struct {
		GetByNameResponse struct {
			GetByNameResult struct {
				Diffgram struct {
					ListByName struct {
						SQL struct {
							ID   string `xml:"ID"`
							Name string `xml:"Name"`
							DOB  string `xml:"DOB"`
							SSN  string `xml:"SSN"`
						} `xml:"SQL"`
					} `xml:"ListByName"`
				} `xml:"diffgram"`
			} `xml:"GetByNameResult"`
		} `xml:"GetByNameResponse"`
	} `xml:"Body"`
}

func (p *PersonResolver) ToResult() Result {
	result := Result{}
	person := p.Body.GetByNameResponse.GetByNameResult.Diffgram.ListByName.SQL

	if person.ID != "" {
		id, _ := strconv.Atoi(person.ID)
		result.ID = id
		result.Name = person.Name
		result.Type = "Person"
		result.Source = "CRCind"
	}

	return result
}
