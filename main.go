package main

import (
	"errors"
	"fmt"
	"local/handlers"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

type App struct {
	Port int
}

func (app *App) init() error {
	port, err := strconv.Atoi(os.Getenv("PORT"))

	if err != nil {
		return errors.New("PORT is not specified or is not a positive integer")
	}

	app.Port = port
	return nil
}

func (app *App) getPort() string {
	return fmt.Sprintf(":%d", app.Port)
}

func main() {
	app := &App{}
	err := app.init()

	if err != nil {
		log.Fatal(err)
		return
	}

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/ping", handlers.PingHandler)
	router.HandleFunc("/search", handlers.SearchHandler)

	err = http.ListenAndServe(app.getPort(), router)
	if err != nil {
		log.Fatal(err)
	}
}
